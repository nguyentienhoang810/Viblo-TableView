//
//  CustomOperation.swift
//  viblo-5
//
//  Created by nguyentienhoang on 10/14/18.
//  Copyright © 2018 Nguyen Tien Hoang. All rights reserved.
//

import UIKit

class DownloadImage: Operation {
    
    enum DownloadState {
        case new, success, failed
    }
    
    var urlString: String?
    var image: UIImage?
    var downloadState: DownloadState = .new
    
    init(urlString: String) {
        self.urlString = urlString
    }
    
    override func main() {
        print("image download state is: ",downloadState)
        if self.isCancelled {
            return
        }
        
        let imageData = NSData(contentsOf: URL(string: self.urlString!)!)
        
        if self.isCancelled {
            return
        }
        
        if (imageData?.length)! > 0 {
            self.image = UIImage(data: imageData! as Data)
            downloadState = .success
            print("image download state is: ",downloadState)
        } else {
            self.image = UIImage(named: "Close")
            downloadState = .failed
        }
    }
}

class ExcutingOperation {
    lazy var downloadQueue:OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Download queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
}
