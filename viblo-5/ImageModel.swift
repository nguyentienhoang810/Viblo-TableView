//
//  ImageModel.swift
//  viblo-5
//
//  Created by nguyentienhoang on 8/14/18.
//  Copyright © 2018 Nguyen Tien Hoang. All rights reserved.
//

import Foundation

struct ImageModel: Decodable {
    var albumId: Int?
    var id: Int?
    var title: String?
    var url: String?
    var thumbnailUrl: String?
}
