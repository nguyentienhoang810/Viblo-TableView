//
//  CustomCell.swift
//  viblo-5
//
//  Created by Nguyen Tien Hoang on 5/27/18.
//  Copyright © 2018 Nguyen Tien Hoang. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    
    var img: ImageModel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "cellID")
        
//        setupLayout()
    }
    
    let excutingOperation = ExcutingOperation()
    
    func setupLayout() {
        contentView.addSubview(cellImage)
        cellImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        cellImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        cellImage.widthAnchor.constraint(equalToConstant: imageSize).isActive = true
        cellImage.heightAnchor.constraint(equalToConstant: imageSize).isActive = true
        imageBottom = cellImage.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        imageBottom.priority = UILayoutPriority(rawValue: 750)
        imageBottom.isActive = true

        contentView.addSubview(cellLabel)
        cellLabel.leadingAnchor.constraint(equalTo: cellImage.trailingAnchor, constant: 5).isActive = true
        cellLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5).isActive = true
        cellLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5).isActive = true
        cellLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5).isActive = true
    }
    
    func defaultLayout() {
        cellImage.image = UIImage(named: "default")
        contentView.addSubview(indicator)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        NSLayoutConstraint.activate([
            indicator.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            indicator.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            indicator.topAnchor.constraint(equalTo: contentView.topAnchor),
            indicator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        indicator.startAnimating()
    }
    
    func reloadData() {
        guard let img = img else { return }
        cellLabel.text = img.title
        
        guard let thumbnailString = img.thumbnailUrl else { return }
        
        let imageDownloadOperation = DownloadImage(urlString: thumbnailString)
        excutingOperation.downloadQueue.addOperation(imageDownloadOperation)
        imageDownloadOperation.completionBlock = {
            DispatchQueue.main.async {
                self.cellImage.image = imageDownloadOperation.image
                self.indicator.stopAnimating()
                self.indicator.removeFromSuperview()
            }
        }
    }
    
    let indicator = UIActivityIndicatorView()
    
    let labelHeight: CGFloat = 30
    
    let cellLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.text = ""
        return label
    }()
    
    let imageSize: CGFloat = 40
    
    let cellImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    var imageBottom = NSLayoutConstraint()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
