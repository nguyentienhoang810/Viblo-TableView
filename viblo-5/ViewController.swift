//
//  ViewController.swift
//  viblo-5
//
//  Created by Nguyen Tien Hoang on 5/27/18.
//  Copyright © 2018 Nguyen Tien Hoang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let mainTableView = MainTableView()
    var cache = URLCache.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        
        fetchImage { (images) in
            self.mainTableView.images = images
            self.mainTableView.reloadData()
        }
    }
    
    func fetchImage(completion: @escaping ([ImageModel]) -> ()) {
        let urlSring = "https://jsonplaceholder.typicode.com/photos"
        guard let url = URL(string: urlSring) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            let decoder = JSONDecoder()
            do {
                guard let data = data else { return }
                let dataArray = try decoder.decode([ImageModel].self, from: data)
                DispatchQueue.main.async {
                    completion(dataArray)
                }
            } catch {
                print(error)
            }
        }.resume()
    }
    
    func setup() {
        self.view.addSubview(mainTableView)
        mainTableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        mainTableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        mainTableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        mainTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }

}

