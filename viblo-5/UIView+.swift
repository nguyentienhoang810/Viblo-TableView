//
//  UIView+.swift
//  viblo-5
//
//  Created by nguyentienhoang on 10/13/18.
//  Copyright © 2018 Nguyen Tien Hoang. All rights reserved.
//

import UIKit

extension UIView {
    
    func activeShimmeAnimation() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.white.cgColor, UIColor.clear.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.7, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.8)
        gradientLayer.frame = self.bounds
        self.layer.mask = gradientLayer
        
        let animation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.duration = 1.5
        animation.fromValue = -self.frame.size.width
        animation.toValue = self.frame.size.width
        animation.repeatCount = .infinity
        
        gradientLayer.add(animation, forKey: "shimmer")
    }
    
    func removeShimmeAnimation() {
        self.layer.removeAllAnimations()
        self.layer.mask = nil
    }
}
